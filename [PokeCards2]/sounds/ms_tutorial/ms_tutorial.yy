{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "ms_tutorial",
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "ms_tutorial.wav",
  "duration": 25.714603,
  "parent": {
    "name": "music",
    "path": "folders/Sounds/music.yy",
  },
}