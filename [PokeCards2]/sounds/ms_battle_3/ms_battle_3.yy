{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "ms_battle_3",
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "ms_battle_3.wav",
  "duration": 163.52313,
  "parent": {
    "name": "music",
    "path": "folders/Sounds/music.yy",
  },
}